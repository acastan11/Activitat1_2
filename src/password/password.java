/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package password;

import java.util.Random;

/**
 *
 * @author Alex Castan
 */
public class password {

    int longitudMax = 10;
    int longitudMin = 6;
    String password = "";

    public password(String password) throws Exception {
        setPassword(password);
        int longitud = password.length();

        if (longitud > 10 || longitud < 6) {
            throw new Exception("La longitud ha de ser entre 6 i 10 caracters");
        }
    }

    public int getLongitudMax() {
        return longitudMax;
    }

    public int setLongitudMax(int longitudMax) {
        this.longitudMax = longitudMax;
        if (longitudMax == 0) {
            return -1;  //-1 incorrecte
        }
        return 1; //1 correcte
    }

    public int getLongitudMin() {
        return longitudMin;
    }

    public void setLongitudMin(int longitudMin) {
        this.longitudMin = longitudMin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /*Creeu el mètode  boolean isStrong() que retorna true si el password és fort,
    i false en qualsevol altre cas. Un password és fort si te com a mínim
    2 caràcters numèrics, 1 minúscula i 1 majúscula.*/
    public boolean isStrong(String contrassenya) {
        boolean resultat = false;
        int nums = 0;
        int majuscules = 0;
        int minuscules = 0;

        for (int i = 0; i < password.length(); i++) {
            char caracter;
            caracter = password.charAt(i);
            if (Character.isDigit(caracter) == true) {
                nums = nums + 1;
            }
        }

        for (int i = 0; i < password.length(); i++) {
            char caracter;
            caracter = password.charAt(i);

            if (caracter >= 'A' && caracter <= 'Z') {
                majuscules++;
            } else if (caracter >= 'a' && caracter <= 'z') {
                minuscules++;

            }
        }

        if (nums > 1 && majuscules > 0 && minuscules > 0) {
            resultat = true;
            System.out.println("Es forta");
        } else {
            System.out.println("Es debil");
        }
        return resultat;
    }

    public String createPassword() {
        char[] caracteresnum;
        char[] caracteresmajus;
        char[] caracteresminus;
        String pass = "";
        int Min = 6;

        caracteresnum = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        caracteresmajus = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        caracteresminus = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
        try {
            int quantitat = (int) (Math.random() * 4) + Min + 1;
            int repet = quantitat;
            int repet2 = 0;

            for (int i = 0; i < repet; i++) {
                repet2 = (int) (Math.random() * 3) + 1;
                switch (repet2) {
                    case 1:
                        pass += caracteresnum[new Random().nextInt(10)];
                        break;
                    case 2:
                        pass += caracteresmajus[new Random().nextInt(26)];
                        break;
                    case 3:
                        pass += caracteresminus[new Random().nextInt(26)];
                        break;
                    default:
                        break;
                }
            }
        } catch (NumberFormatException ex) {
            ex.printStackTrace(System.out);

        }
        System.out.println(pass);
        return pass;
    }

}
