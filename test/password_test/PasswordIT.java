/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package password_test;

import password.password;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alex Castan
 */
public class PasswordIT {

    public PasswordIT() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getLongitudMax method, of class Password.
     */
    @Test
    public void testPassword() throws Exception {
        System.out.println("testPassword");
        password pass = new password("Ue5yDh2");
        int expMaxResult = 10;
        int expMinResult = 6;
        String expPassword = "Ue5yDh2";
        int result = pass.getLongitudMax();
        int result2 = pass.getLongitudMin();
        String result3 = pass.getPassword();
        assertEquals(expMaxResult, result);
        assertEquals(expMinResult, result2);
        assertEquals(expPassword, result3);
        // TODO review the generated test code and remove the default call to fail.

    }

    @Test
    public void testEsStrong() throws Exception {
        System.out.println("esStrong");
        String contrassenya = "U7hJ9uh";
        password pass = new password("U7hJ9uh");
        boolean result;
        result = pass.isStrong(contrassenya);
        assertEquals(true,result);
        // TODO review the generated test code and remove the default call to fail.

    }

}
